/* Listen post messages from clients */

self.addEventListener('message', (event) => {
	if (!event.data || !event.data.type) {
		return;
	}
	if (event.data.type === 'FORCE_SKIP_WAITING') {
		console.log('Skip waiting');
		self.skipWaiting().catch(() => {});
	}
	if (event.data.type === 'FORCE_CLIENTS_CLAIM') {
		console.log('Clients Claim');
		self.clients.claim().catch(() => {});
	}
});
