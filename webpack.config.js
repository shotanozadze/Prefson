const webpack = require('webpack');
const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { merge } = require('webpack-merge');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const modeConfig = (env) => require(`./build-utils/webpack.${env.mode}.js`)(env);

const webcomponentsjs = './node_modules/@webcomponents/webcomponentsjs';

const polyfills = [
	{
		from: resolve(`${webcomponentsjs}/webcomponents-bundle.js`),
		to: 'vendor'
	},
	{
		from: resolve(`${webcomponentsjs}/webcomponents-bundle.js.map`),
		to: 'vendor'
	},
	{
		from: resolve(`${webcomponentsjs}/webcomponents-loader.js`),
		to: 'vendor'
	},
	{
		from: resolve(`${webcomponentsjs}/bundles`),
		to: 'vendor/bundles'
	},
	{
		from: resolve(`${webcomponentsjs}/custom-elements-es5-adapter.js`),
		to: 'vendor'
	}
];

const assets = [
	{ from: 'src/assets', to: 'assets/' }
];

let plugins = [
	new webpack.ProgressPlugin(),
	new HtmlWebpackPlugin({
		filename: 'index.html',
		template: './src/index.html',
		minify: {
			collapseWhitespace: true,
			minifyCSS: true,
			minifyJS: true
		}
	}),
	new CopyWebpackPlugin(
		{
			patterns: [...polyfills, ...assets]
		},
		{
			ignore: ['.DS_Store']
		}
	)
];

const serviceWorkerPlugin = new WorkboxPlugin.GenerateSW({
	exclude: [/node_modules\/(.*)\.(?!(css$))/, /assets\/(.*)\.(jpg|jpeg|png|json)$/],
	runtimeCaching: [
		{
			urlPattern: /^.*\.(config\.js|json)$/,
			handler: 'NetworkFirst'
		},
		{
			urlPattern: /^.*\.(css|html)$/,
			handler: 'StaleWhileRevalidate'
		},
		{
			urlPattern: /^.*\.(woff2|woff|ttf|eot|jpg|jpeg|png|svg)$/,
			handler: 'CacheFirst'
		}
	],
	swDest: './service-worker.js',
	clientsClaim: true,
	skipWaiting: true,
	navigateFallback: `/index.html`,
	importScripts: ['./service-worker-events.js']
});

module.exports = ({ mode, presets }, argv) => {
	if (argv.env.mode === 'production') {
		plugins = [new CleanWebpackPlugin(), ...plugins, serviceWorkerPlugin];
	}

	return merge(
		{
			mode,
			output: {
				filename: '[name].[chunkhash:8].js',
				path: resolve(process.cwd(), 'dist')
			},
			devServer: {
				watchOptions: {
					ignored: [
						resolve(__dirname, 'dist'),
						resolve(__dirname, 'node_modules')
					]
				},
				historyApiFallback: true
			},
			optimization: {
				minimizer: [new UglifyJsPlugin()]
			},
			module: {
				rules: [
					{
						test: /\.js$/,
						loader: 'string-replace-loader',
						options: {
							multiple: [
								{ search: '__BREAKPOINT-LARGE__', replace: '1920px', flags: 'g' },
                                { search: '__BREAKPOINT-M-LARGE__', replace: '1200px', flags: 'g' },
								{ search: '__BREAKPOINT-MEDIUM__', replace: '1024px', flags: 'g' },
								{ search: '__BREAKPOINT-SMALL__', replace: '768px', flags: 'g' },
								{ search: '__BREAKPOINT-M-SMALL__', replace: '660px', flags: 'g' },
                                { search: '__BREAKPOINT-X-SMALL__', replace: '500px', flags: 'g' }
							]
						}
					},
					{
						test: /\.js$/,
						exclude: /node_modules/,
						loader: 'babel-loader',
						options: {
							plugins: ['@babel/plugin-syntax-dynamic-import'],
							presets: [
								[
									'@babel/preset-env',
									{
										useBuiltIns: 'usage',
										targets: '>1%, not dead, not ie 11'
									}
								]
							]
						}
					}
				]
			},
			plugins
		},
		modeConfig({ mode, presets })
	);
};
