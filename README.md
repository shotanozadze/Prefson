# Prefson
* **Back სერვერის გაშვება:**
  * server/ დირექტორიიდან ვუშვებთ ბრძანებებს: npm install, node server.js
  * სერვერი გაშვებული იქნება 2017 პორტზე
* **Front სერვერის გაშვება**
  * root დირექტორიიდან ვუშვებთ ბრძანებებს: npm install, npm run build, npm run dev
  * აპლიკაცია გაშვებული იქნება 8080 პორტზე
