const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();
const cors = require('cors');
const { createHash } = require('crypto');

app.use(jsonParser);
app.use(express.static(__dirname + '/'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.text());
app.use(cors());

const Connection = require('./connection');
const Config = require('./config');
const { ObjectId } = require('mongodb');

Config.set();
const MongoClient = require('mongodb').MongoClient;

MongoClient.connect(Config.getUri(), Config.getOptions(), (error, client) => {
    if (error){
        return console.log(error);
    }
    console.log(`Connected to Database`);
    Connection.set(client.db(Config.getDB()));
});

app.listen(Config.getPort(), () => {
    console.log(`Server running on port ${Config.getPort()}`);
});

app.post('/login', jsonParser, async (request, response) => {
    const data = request.body;
    const users = Connection.get().collection(Config.getUsersCollection());
    const user = await users.findOne({ username: data.username });
    if (user) {
        if (user.password === data.password) {
            const tok = createHash('sha256').update(user._id + user.password).digest('hex');
            response.json(JSON.stringify({success: true, message: 'Welcome to Prefson!', cx: user._id, cxToken: tok}));
            response.end();
        } else {
            response.json(JSON.stringify({success: false, message: "Sorry, that password isn't correct, try again."}));
            response.end();
        }
    } else {
        response.json(JSON.stringify({success: false, message: "Sorry, we couldn't find an account with that username."}));
        response.end();
    }
});

app.post('/checkuser', jsonParser, async (request, response) => {
    const data = request.body;
    const users = Connection.get().collection(Config.getUsersCollection());
    const user = await users.findOne({ _id: ObjectId(data.cx) });
    if (user) {
        const tok = createHash('sha256').update(user._id + user.password).digest('hex');
        if (tok === data.cxToken) {
            let userInfo = user;
            delete userInfo.password;
            response.json(JSON.stringify({success: true, message: 'Welcome to Prefson!', cid: user}));
            response.end();
        } else {
            response.json(JSON.stringify({success: false, message: "Sorry, that password isn't correct, try again."}));
            response.end();
        }
    } else {
        response.json(JSON.stringify({success: false, message: "Sorry, we couldn't find an account with that id."}));
        response.end();
    }
});

app.post('/register', jsonParser, async (request, response) => {
    const data = request.body;
    const users = Connection.get().collection(Config.getUsersCollection());
    const fUsername = await users.findOne({ username: data.username });
    const fMail = await users.findOne({ mail: data.mail });
    if (!fUsername && !fMail) {
        users.insertOne(data, (err, result) => {
            if (err) {
                response.json(JSON.stringify({success: false, message: 'Sorry, something went wrong, try again.'}));
                response.end();
                throw err;
            }
            response.json(JSON.stringify({success: true, message: 'Congratulations, your Prefson account has been successfully created.'}));
            response.end();
        });
    } else {
        if (fUsername) {
            response.json(JSON.stringify({success: false, message: 'Sorry, username is already taken, try another.'}));
            response.end();
        } else {
            response.json(JSON.stringify({success: false, message: 'Sorry, email address is already registered, try another.'}));
            response.end();
        }
    }
});

app.post('/chats', jsonParser, async (request, response) => {
    const data = request.body;
    const chats = Connection.get().collection(Config.getChatsCollection());
    chats.aggregate([
        {
            $match: {
                isChannel: data.isChannel,
                "users.userId": ObjectId(data.id)
            }
        },
        {
            $lookup:
            {
                from: 'users',
                localField: 'users.userId',
                foreignField: '_id',
                as: 'participants'
            }
        },
        {
            $project:
            {
                'participants.password': 0,
                'participants.mail': 0,
                'participants.connections': 0
            }
        }
    ]).toArray((err, res) => {
        if (err) throw err;
        response.json(JSON.stringify({cid: res}));
        response.end();
    });
});

app.post('/messages', jsonParser, async (request, response) => {
    const data = request.body;
    const messages = Connection.get().collection(Config.getMessagesCollection());
    const chats = Connection.get().collection(Config.getChatsCollection());
    await chats.updateOne(
        { _id: ObjectId(data.id) },
        {
            $set: {
                'users.$[e].seen': new Date()
            },
        },
        {   arrayFilters: [
                { 'e.userId': ObjectId(data.userId) }
            ]
        }
    );

    messages.aggregate([
        {
            $match: {
                chatId: ObjectId(data.id)
            }
        },
        {
            $lookup:
            {
                from: 'users',
                localField: 'senderId',
                foreignField: '_id',
                as: 'user'
            }
        },
        {
            $project:
            {
                'user.password': 0,
                'user.mail': 0,
                'user._id': 0,
                'user.connections': 0
            }
        },
        {
            $sort:
            {
                time: -1
            }
        }
    ]).limit(data.nums).toArray((error, result) => {
        if (error) throw error;
        messages.find({chatId: ObjectId(data.id)}).toArray((err, res) => {
            if (err) throw err;
            response.json(JSON.stringify({cid: result, count: res.length}));
            response.end();
        });
    });
});

app.post('/sendMessage', jsonParser, async (request, response) => {
    const data = request.body;
    const messages = Connection.get().collection(Config.getMessagesCollection());
    const chats = Connection.get().collection(Config.getChatsCollection());
    const msg = {
        senderId: ObjectId(data.senderId),
        chatId: ObjectId(data.chatId),
        message: data.message,
        time: new Date()
    };
    messages.insertOne(msg, async (err, result) => {
        if (err) {
            response.json(JSON.stringify({success: false}));
            response.end();
            throw err;
        }
        await chats.updateOne(
            { _id: ObjectId(data.chatId) },
            {
                $set: {
                    'update': new Date()
                }
            }
        );
        response.json(JSON.stringify({success: true}));
        response.end();
    });
});

app.post('/searchUsers', jsonParser, async (request, response) => {
    const query = request.body.query;
    const users = Connection.get().collection(Config.getUsersCollection());
    users.aggregate([
        {
            $match: {
                username: {
                    $regex: query
                }
            }
        },
        {
            $project:
            {
                'password': 0,
                'mail': 0,
                '_id': 0,
                'connections': 0
            }
        }
    ]).toArray((err, res) => {
        if (err) throw err;
        response.json(JSON.stringify(res));
        response.end();
    });
});

app.post('/searchChannels', jsonParser, async (request, response) => {
    const query = request.body.query;
    const chats = Connection.get().collection(Config.getChatsCollection());
    chats.aggregate([
        {
            $match: {
                chatname: {
                    $regex: query
                },
                isChannel: true,
                isPublic: true
            }
        }
    ]).toArray((err, res) => {
        if (err) throw err;
        response.json(JSON.stringify(res));
        response.end();
    });
});

app.post('/updateProfile', jsonParser, async (request, response) => {
    const data = request.body;
    const users = Connection.get().collection(Config.getUsersCollection());
    if (data.password.length > 0) {
        const user = await users.updateOne(
            { username: data.username },
            {
                $set: {
                    'firstname': data.firstname,
                    'lastname': data.lastname,
                    'avatar': data.avatar,
                    'password': data.password
                }
            }
        );
        response.json(JSON.stringify(user));
        response.end();
    } else {
        const user = await users.updateOne(
            { username: data.username },
            {
                $set: {
                    'firstname': data.firstname,
                    'lastname': data.lastname,
                    'avatar': data.avatar
                }
            }
        );
        response.json(JSON.stringify(user));
        response.end();
    }
});

app.post('/connectUser', jsonParser, async (request, response) => {
    const data = request.body;
    const users = Connection.get().collection(Config.getUsersCollection());
    const user1 = await users.findOne({ username: data.user1 });
    const user2 = await users.findOne({ username: data.user2 });
    if (!user1.connections.includes(data.user2) && !user2.connections.includes(data.user1)) {
        await users.updateOne(
            { username: data.user1 },
            {
                $push: {
                    connections: data.user2
                }
            }
        );

        await users.updateOne(
            { username: data.user2 },
            {
                $push: {
                    connections: data.user1
                }
            }
        );

        const chats = Connection.get().collection(Config.getChatsCollection());
        const chat = {
            chatname: '',
            isChannel: false,
            isPublic: false,
            users: [{
                    userId: ObjectId(user1._id),
                    seen: new Date(0)
                }, {
                    userId: ObjectId(user2._id),
                    seen: new Date(0)
                }
            ],
            update: new Date()
        };

        chats.insertOne(chat, (err, result) => {
            if (err) {
                response.json(JSON.stringify({success: false}));
                response.end();
                throw err;
            }
            response.json(JSON.stringify({success: true}));
            response.end();
        });
    } else {
        response.json(JSON.stringify({success: false}));
        response.end();
    }
});

app.post('/createChannel', jsonParser, async (request, response) => {
    const data = request.body;
    const chats = Connection.get().collection(Config.getChatsCollection());
    const chat = {
        chatname: data.chatname,
        isChannel: true,
        isPublic: data.isPublic,
        users: [
            {
                userId: ObjectId(data.userId),
                seen: new Date(0)
            }
        ],
        update: new Date()
    };
    if (chat.isPublic) {
        const fChat = await chats.findOne({chatname: chat.chatname, isPublic: true});
        if (fChat) {
            response.json(JSON.stringify({success: false}));
            response.end();
        } else {
            chats.insertOne(chat, (err, result) => {
                if (err) {
                    response.json(JSON.stringify({success: false}));
                    response.end();
                    throw err;
                }
                response.json(JSON.stringify({success: true}));
                response.end();
            });
        }
    } else {
        chats.insertOne(chat, (err, result) => {
            if (err) {
                response.json(JSON.stringify({success: false}));
                response.end();
                throw err;
            }
            response.json(JSON.stringify({success: true}));
            response.end();
        });
    }
});

app.post('/joinChannel', jsonParser, async (request, response) => {
    const data = request.body;
    const chats = Connection.get().collection(Config.getChatsCollection());
    const chat = await chats.findOne({_id: ObjectId(data.channel)});
    if (!chat.users.map((e) => e.userId.toString()).includes(data.user)) {
        await chats.updateOne(
            { _id: ObjectId(data.channel) },
            {
                $push: {
                    users: {
                        userId: ObjectId(data.user),
                        seen: new Date(0)
                    }
                }
            }
        );
        response.json(JSON.stringify({success: true}));
        response.end();
    } else {
        response.json(JSON.stringify({success: false}));
        response.end();
    }
});

app.post('/leaveChannel', jsonParser, async (request, response) => {
    const data = request.body;
    const chats = Connection.get().collection(Config.getChatsCollection());
    const chat = await chats.findOne({_id: ObjectId(data.chatId)});
    if (chat.users.map((e) => e.userId.toString()).includes(data.userId)) {
        await chats.updateOne(
            { _id: ObjectId(data.chatId) },
            {
                $pull: {
                    users: {
                        userId: ObjectId(data.userId)
                    }
                }
            }
        );
        response.json(JSON.stringify({success: true}));
        response.end();
    } else {
        response.json(JSON.stringify({success: false}));
        response.end();
    }
});

app.post('/addFriendToChannel', jsonParser, async (request, response) => {
    const data = request.body;
    const users = Connection.get().collection(Config.getUsersCollection());
    const user = await users.findOne({username: data.username});
    const chats = Connection.get().collection(Config.getChatsCollection());
    const chat = await chats.findOne({_id: ObjectId(data.chatId)});
    if (!chat.users.map((e) => e.userId.toString()).includes(user._id.toString())) {
        await chats.updateOne(
            { _id: ObjectId(data.chatId) },
            {
                $push: {
                    users: {
                        userId: ObjectId(user._id),
                        seen: new Date(0)
                    }
                }
            }
        );
        response.json(JSON.stringify({success: true}));
        response.end();
    } else {
        response.json(JSON.stringify({success: false}));
        response.end();
    }
});

const { Server } = require('socket.io');
const { createServer } = require('http');

const httpServer = createServer();
const io = new Server(httpServer, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    }
});

io.on('connection', (socket) => {
    if (socket.username) {
        socket.broadcast.emit('connected', socket.username);
        socket.on('disconnect', () => {
            setTimeout(() => {
                let userDisconnected = true;
                for (let [id, sock] of io.of("/").sockets) {
                    if (sock.username === socket.username) {
                        userDisconnected = false;
                    }
                }

                if (userDisconnected) {
                    socket.broadcast.emit('disconnected', socket.username);
                }
            }, 500);
        });

    }

    let users = [];
    for (let [id, sock] of io.of("/").sockets) {
        if (sock.username) {
            users.push({
                username: sock.username
            });
        }
    }
    socket.emit('users', users);

    socket.on('send message', (info) => {
        socket.broadcast.emit('get message_' + info.chatId, info.chatId);
    });
});

io.on('connect', (socket) => {
    socket.username = socket.handshake.auth.username;
});

httpServer.listen(Config.getSocketPort());