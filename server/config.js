class Config{
    static set(){
        Config.port = 2017;
        Config.socketPort = 3000;
        Config._db = 'Prefson';
        Config.usersCollection = 'users';
        Config.chatsCollection = 'chats';
        Config.messagesCollection = 'messages';
        Config.user = 'snoza17';
        Config.pass = 'snoza17';
        Config.uri_info = '@cluster0.wkz83.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
        Config.uri = 'mongodb+srv://' + Config.user + ':' + Config.pass + Config.uri_info;
        Config.options = {
            useNewUrlParser: true, 
            useUnifiedTopology: true
        };
    }

    static getPort(){
        return Config.port;
    }

    static getSocketPort() {
        return Config.socketPort;
    }

    static getDB(){
        return Config._db;
    }

    static getUsersCollection(){
        return Config.usersCollection;
    }

    static getChatsCollection() {
        return Config.chatsCollection;
    }

    static getMessagesCollection() {
        return Config.messagesCollection;
    }

    static getUri(){
        return Config.uri;
    }

    static getOptions(){
        return Config.options;
    }
}

module.exports = Config;
