const initApplication = () => {
	import('./sn-root.js').then(() => {
		const body = document.querySelector('body');
		const root = document.createElement('sn-root');
		body.appendChild(root);
	});
};

initApplication();
