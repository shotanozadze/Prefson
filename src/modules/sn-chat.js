import { LitElement, html, css } from 'lit';
import { styleMap } from 'lit/directives/style-map.js';
import { Manager } from 'socket.io-client';
import { Server } from '../common/constants';
import { classMap } from 'lit/directives/class-map.js';
import CryptoJS from 'crypto-js';
import '../components/sn-chat-message';

class SnChat extends LitElement {
	static get is() {
		return 'sn-chat';
	}

	static get styles() {
		return css`
			.wrapper {
                position: relative;
				margin: 0 auto;
                height: calc(var(--vh, 1vh) * 100);
                display: grid;
                grid-template-rows: 64px 1fr 156px;
			}

            .user-header-wrapper {
                padding: 0 24px;
                background: #252727;
                box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
            }

            .user-header {
                display: flex;
                flex-direction: row;
                justify-content: space-between;
            }

            .header-info {
                display: flex;
                width: 100%;
                cursor: default;
            }

            .user_avatar {
                position: relative;
                width: 40px;
                height: 40px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                border-radius: 10px;
                margin-top: 12px;
            }

            .channel_icon {
                position: relative;
                width: 32px;
                height: 32px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                border-radius: 10px;
                margin-top: 16px;
            }

            .user-name-wrapper {
                display: flex;
                flex-direction: column;
                margin: 12px 16px;
                user-select: none;
            }

            .user-name {
                font-family: pbold-1;
                color: #eaedde;
                font-size: 17px;
                line-height: 21px;
            }

            .user-username {
                font-family: prefson-1;
                color: #71767a;
                font-size: 16px;
                margin-top: 2px;
            }

            .channel-name {
                font-family: pbold-1;
                color: #eaedde;
                font-size: 20px;
                line-height: 40px;
            }

            .chat-messages {
                display: flex;
                flex-direction: column-reverse;
                overflow-y: overlay;
            }

            .chat-messages::-webkit-scrollbar {
                width: 8px;
            }

            .chat-messages::-webkit-scrollbar-track {
                box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            }

            .chat-messages::-webkit-scrollbar-thumb {
                background-color: rgb(0 0 0 / 41%);
                border-radius: 4px;
            }

            .send-message-wrapper {
                position: relative;
                padding: 12px 24px;
                border-top: 1px solid #252727;
                display: flex;
                background: #191a1a;
            }

            .send-message-field {
                padding: 8px 60px 8px 16px;
                font-family: prefson-1;
                font-size: 15px;
                border-radius: 12px;
                border: 1px solid #353535;
                background: #252727;
                outline: none;
                color: #c9cec2;
                height: 48px;
                width: calc(100% - 32px);
            }

            .send-button {
                width: 32px;
                height: 32px;
                position: absolute;
                right: 40px;
                top: 30px;
                background-color: rgb(0, 150, 136);
                border-radius: 30px;
                cursor: pointer;
            }

            .send-button img {
                width: 16px;
                height: 16px;
                padding: 8px;
            }

            .header-dropdown {
                width: 24px;
                height: 24px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/rArrow.png);
                transform: rotate(90deg);
                transition: 0.5s;
                margin: auto;
                cursor: pointer;
            }

            .dropup {
                transform: rotate(-90deg);
            }

            .dropdown_active {
                grid-template-rows: 120px 1fr 156px;
            }

            .props-wrapper {
                width: 100%;
                display: flex;
                justify-content: space-between;
                border-top: 1px solid #191a1a;
                padding-top: 8px;
            }

            .add-friend {
                display: flex;
            }

            .add-friend-field {
                padding: 8px;
                font-family: prefson-1;
                font-size: 15px;
                border-radius: 8px;
                border: 1px solid rgb(70, 84, 87);
                background: rgb(47, 64, 65);
                outline: none;
                color: rgb(201, 206, 194);
                height: 19px;
            }

            .add-friend-butt {
                margin: 0 8px;
                color: #faead7;
                font-family: pbold-1;
                font-size: 15px;
                user-select: none;
                padding: 10px;
                background: #011f26;
                text-align: center;
                border-radius: 8px;
                cursor: pointer;
                box-shadow: 0px 0px 2px #ffffff42;
            }

            .leave-butt {
                color: red;
                font-family: pbold-1;
                font-size: 15px;
                user-select: none;
                padding: 10px;
                background: #fdedee;
                text-align: center;
                border-radius: 4px;
                cursor: pointer;
                border: 1px solid red;
            }

            @media (max-width: __BREAKPOINT-M-SMALL__) {
                .user-header-wrapper {
                    padding: 0 16px;
                }

                .send-message-wrapper {
                    padding: 12px 16px;
                }

                .send-button {
                    right: 24px;
                }
            }
		`;
	}

	render() {
		return html`
            <div class="wrapper ${classMap({dropdown_active: this._dropActive})}">
                <div class="user-header-wrapper">
                    <div class="user-header">
                        <div class="header-info">
                            <div
                                class="${classMap({
                                    channel_icon: this.chat.isChannel,
                                    user_avatar: !this.chat.isChannel
                                })}"
                                style="${styleMap({'background-image': 'url(' + this.chat.avatar + ')'})}">
                            </div>
                            <div class="user-name-wrapper">
                                ${
                                    this.chat.isChannel ? html`
                                        <div class="channel-name">${this.chat.username}</div>
                                    ` :
                                    html`
                                        <div class="user-name">${this.chat.fullname}</div>
                                        <div class="user-username">${this.chat.username}</div>
                                    `
                                }
                            </div>
                        </div>
                        ${
                            this.chat.isChannel ? html`
                                <div
                                    class="header-dropdown
                                    ${classMap({dropup: this._dropActive})}"
                                    @click=${this._onDropDown.bind(this)}
                                    >
                                </div>
                            ` : ``
                        }
                    </div>
                    ${
                        this._dropActive ? html`
                            <div class="props-wrapper">
                                <div class="add-friend">
                                    <input type="text" placeholder="Username" class="add-friend-field" id="friend-username">
                                    <div class="add-friend-butt" @click=${this._addFriend.bind(this)}>Add</div>
                                </div>
                                <div class="leave-butt" @click=${this._onLeave.bind(this)}>Leave</div>
                            </div>
                        ` : ``
                    }
                </div>
                
                <div class="chat-messages" id="chat-box">
                    ${
                        this._messages.map((elem) => {
                            return html`
                                <sn-chat-message .messageInfo=${elem} .chatId=${this.chat.chatId}></sn-chat-message>
                            `;
                        })
                    }
                </div>

                <div class="send-message-wrapper">
                    <input type="text" placeholder="Type a message..." class="send-message-field" id="sn-send-message" @keypress=${this._onSendPress.bind(this)}>
                    <div class="send-button" @click=${this._onSendClick.bind(this)}>
                        <img src="/assets/images/send.png">
                    </div>
                </div>
            </div>
		`;
	}

    static get properties() {
        return {
            chat: {
                type: Object
            },
            user: {
                type: Object
            },
            _messages: {
                type: Array
            },
            _loadMessages: {
                type: Number
            },
            _msgCount: {
                type: Number
            },
            _dropActive: {
                type: Boolean
            }
        }
    }

    constructor() {
        super();
        this.chat = undefined;
        this.user = undefined;
        this._messages = [];
        this._loadMessages = 20;
        this._msgCount = 0;
        this._dropActive = false;
    }

    firstUpdated(_changedProperties) {
        super.firstUpdated(_changedProperties);
        this._fetchChatMessages(this.chat.chatId);

        const manager = new Manager(Server.SocketsURI);
        const socket = manager.socket("/");
        socket.on('get message_' + this.chat.chatId, (info) => {
            if (info === this.chat.chatId) {
                this._fetchChatMessages(this.chat.chatId);
            }
        });

        const box = this.shadowRoot.getElementById('chat-box');
        box.addEventListener('scroll', () => {
            if (box.offsetHeight - box.scrollTop + 1 >= box.scrollHeight) {
                if (this._msgCount >= this._loadMessages) {
                    this._loadMessages += 20;
                    this._fetchChatMessages(this.chat.chatId);
                }
            }
        });
    }

    _fetchChatMessages(chatId) {
        fetch(Server.URI + Server.Messages, {
            method: "POST",
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(
                {
                    id: chatId,
                    nums: this._loadMessages,
                    userId: this.user?._id
                }
            )
        })
        .then((blob) => blob.json())
        .then((data) => {
            const dt = JSON.parse(data);
            let elems = dt.cid;
            this._msgCount = dt.count;
            this._messages = [];
            elems.forEach((elem) => {
                if (elem.user?.length > 0) {
                    const user = elem.user[0];
                    if (!user.avatar) user.avatar = '/assets/images/user.png'; 
                    const msg = {
                        username: '@' + user.username,
                        fullname: user.firstname + ' ' + user.lastname,
                        avatar: user.avatar,
                        message: elem.message,
                        time: elem.time
                    };
                    this._messages = [...this._messages, msg];
                }
            });
        });
    }

    _onSendClick(e) {
        const msg = this.shadowRoot.getElementById('sn-send-message').value;
        if (msg.length === 0) return;
        const senderId = this.user._id;
        const chatId = this.chat.chatId;
        const encryptedMsg = this._encryptAES(msg, chatId);

        fetch(Server.URI + Server.SendMessage, {
            method: "POST",
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(
                {
                    senderId: senderId,
                    chatId: chatId,
                    message: encryptedMsg
                }
            )
        })
        .then((blob) => blob.json())
        .then((data) => {
            const res = JSON.parse(data);
            if (res.success) {
                const manager = new Manager(Server.SocketsURI);
                const socket = manager.socket("/");
                socket.emit('send message', { chatId: chatId });
                this.shadowRoot.getElementById('sn-send-message').value = '';
            }
        });
    }

    _onSendPress(e) {
        if (e.key === 'Enter') {
            this._onSendClick(e);
        }
    }

    _encryptAES (text, key) {
        return CryptoJS.AES.encrypt(text, key).toString();
    };

    _onDropDown() {
        if (this._dropActive) {
            this._dropActive = false;
        } else {
            this._dropActive = true;
        }
    }

    _addFriend() {
        const friendUsername = this.shadowRoot.getElementById('friend-username').value;
        if (friendUsername?.length === 0) return;
        if (this.user.connections.includes(friendUsername)) {
            fetch(Server.URI + Server.AddFriendToChannel, {
                method: "POST",
                mode: 'cors',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify(
                    {
                        chatId: this.chat.chatId,
                        username: friendUsername
                    }
                )
            })
            .then((blob) => blob.json())
            .then((data) => {
                const dt = JSON.parse(data);
                if (dt.success) {
                    location.reload();
                }
            });
        }
    }

    _onLeave() {
        fetch(Server.URI + Server.LeaveChannel, {
            method: "POST",
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(
                {
                    chatId: this.chat.chatId,
                    userId: this.user._id
                }
            )
        })
        .then((blob) => blob.json())
        .then((data) => {
            const dt = JSON.parse(data);
            if (dt.success) {
                location.reload();
            }
        });
    }

    updateValues(chat) {
        this._messages = [];

        setTimeout(() => {
            this.chat = chat;
            this._fetchChatMessages(this.chat.chatId);
        }, 300);
    }
}

window.customElements.define(SnChat.is, SnChat);
