import { LitElement, html, css } from 'lit';
import '../components/sn-login';
import '../components/sn-register';
import '../components/sn-header';
import '../components/sn-user-box';
import '../components/sn-chat-box';
import '../components/sn-search-user';
import '../components/sn-search-channel';
import '../components/sn-welcome';
import '../components/sn-profile';
import './sn-chat';
import { CookieParamKeys, Server, FormComponentTypes } from '../common/constants';
import { Manager } from 'socket.io-client';
import { getCookie } from '../common/utils';

class SnForm extends LitElement {
	static get is() {
		return 'sn-form';
	}

	static get styles() {
		return css`
			.wrapper {
				margin: 0px auto;
                height: 100%;
			}

            .container {
                width: 100%;
                height: 100vh;
                display: flex;
            }

            .left-container {
                position: relative;
				margin: 0 auto;
                height: calc(calc(var(--vh, 1vh) * 100) - 64px);
                width: 400px;
                border-right: 1px solid #1a3339;
                display: grid;
                grid-template-rows: 100px 1fr;
            }

            .chat-box-wrapper {
                overflow-y: overlay;
            }

            .chat-box-wrapper::-webkit-scrollbar {
                width: 8px;
            }

            .chat-box-wrapper::-webkit-scrollbar-track {
                box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            }

            .chat-box-wrapper::-webkit-scrollbar-thumb {
                background-color: rgb(0 0 0 / 41%);
                border-radius: 4px;
            }

            .service-form {
                width: calc(100% - 400px);
                background: #191a1a;
                overflow-y: overlay;
            }

            .service-form::-webkit-scrollbar {
                width: 8px;
            }

            .service-form::-webkit-scrollbar-track {
                box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            }

            .service-form::-webkit-scrollbar-thumb {
                background-color: rgb(0 0 0 / 41%);
                border-radius: 4px;
            }

            .search-results {
                display: grid;
                grid-template-columns: 1fr 1fr;
                padding: 0 40px;
                margin-bottom: 200px;
            }

            .search-error {
                display: flex;
                background: #fdeeee;
                padding: 12px 16px;
                margin: 16px 24px;
                font-family: prefson-1;
                font-size: 12px;
                border-radius: 4px;
                border: 1px solid #e22820;
                color: #141111;
            }

            .search-error-icon {
                width: 20px;
                height: 20px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/error.png);
                margin-right: 12px;
            }

            @media (max-width: __BREAKPOINT-M-LARGE__) {
                .search-results {
                    display: grid;
                    grid-template-columns: 1fr;
                    padding: 0;
                }
            }

            @media (max-width: __BREAKPOINT-MEDIUM__) {
                .left-container {
                    width: 350px;
                }

                .service-form {
                    width: calc(100% - 350px);
                }
            }

            @media (max-width: __BREAKPOINT-SMALL__) {
                :host(:not([showForm])) .service-form {
                    display: none;
                }

                :host(:not([showForm])) .left-container {
                    width: 100%;
                    border-right: 0;
                }

                :host([showForm]) .service-form {
                    width: 100%;
                }

                :host([showForm]) .left-container {
                    display: none;
                }
            }
		`;
	}

	render() {
		return html`
            <sn-header
                .user="${this._checkInfo}"
                @searchUsers=${this._searchUsers.bind(this)}
                @searchChannels=${this._searchChannels.bind(this)}>
            </sn-header>
            <div class="wrapper">
                ${this._renderLoginPage()}
                ${
                    this._checkInfo ? html`
                        <div class="container">
                            <div class="left-container">
                                <sn-user-box
                                    .fullname=${this._checkInfo?.cid.firstname + ' ' + this._checkInfo?.cid.lastname}
                                    .username=${'@' + this._checkInfo?.cid.username}
                                    .avatar=${this._checkInfo?.cid.avatar}
                                    @click=${() => this._onProfileClick()}>
                                </sn-user-box>
                                <div class="chat-box-wrapper">
                                    <sn-chat-box
                                        .userId=${this._userId}
                                        .isChannel=${true}
                                        @chatbox-click=${(e) => this._chatBoxClick(e)}
                                        >
                                    </sn-chat-box>
                                    <sn-chat-box
                                        .userId=${this._userId}
                                        .isChannel=${false}
                                        @chatbox-click=${(e) => this._chatBoxClick(e)}>
                                    </sn-chat-box>
                                </div>
                            </div>
                            <div class="service-form">${this._renderForm()}</div>
                        </div>   
                    ` : ``
                }
            </div>
		`;
	}

    static get properties() {
        return {
            _logged: {
                type: Boolean
            },
            _registerForm: {
                type: Boolean
            },
            _checkInfo: {
                type: Object
            },
            _openChat: {
                type: Object
            },
            _chat: {
                type: Object
            },
            _searchUsersResults: {
                type: Array
            },
            _searchChannelResults: {
                type: Array
            },
            _showElement: {
                type: String
            },
            _userId: {
                type: String
            },
            showForm: {
                type: Boolean,
                reflect: true
            }
        }
    }

    constructor() {
        super();

        this._elementMap = {
            [FormComponentTypes.Chat]: this._getChatElement.bind(this),
            [FormComponentTypes.Search]: this._getSearchElement.bind(this),
            [FormComponentTypes.Welcome]: this._getWelcomeElement.bind(this),
            [FormComponentTypes.Profile]: this._getProfileElement.bind(this)
        };

        this.showForm = false;
        this._logged = true;
        this._registerForm = false;
        this._openChat = undefined;
        this._messages = [];
        this._chat = {};
        this._searchUsersResults = [];
        this._searchChannelResults = [];
        this._showElement = FormComponentTypes.Welcome;
        this._userId = undefined;
    }

    firstUpdated(_changedProperties) {
        super.firstUpdated(_changedProperties);
        this._checkUser();
    }

    _executeLogin(e) {
        document.cookie = CookieParamKeys.CX + '=' + e.detail.cx;
        document.cookie = CookieParamKeys.CXTOKEN + '=' + e.detail.cxToken;
        this._logged = true;
        this._checkUser();
    }

    _registerClick(e) {
        this._registerForm = true;
    }

    _checkUser() {
        const cx = getCookie(CookieParamKeys.CX);
        const tok = getCookie(CookieParamKeys.CXTOKEN);

        if (!cx || !tok) {
            this._logged = false;
            return;
        }
        fetch(Server.URI + Server.CheckUser, {
            method: "POST",
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(
                {
                    cx: cx,
                    cxToken: tok
                }
            )
        })
        .then((blob) => blob.json())
        .then((data) => {
            const res = JSON.parse(data);
            if (res.success) {
                this._logged = true;
                this._checkInfo = res;
                this._userId = res.cid._id;
                this._onLoginSocket(res.cid.username); 
            } else {
                this._logged = false;
                this._checkInfo = undefined;
            }
        });
    }

    _renderLoginPage() {
        if (this._registerForm) return this._getRegisterElement();
        if (!this._registerForm && !this._logged) return this._getLoginElement();
    }

    _renderForm() {
        if (this._showElement !== FormComponentTypes.Welcome) {
            this.showForm = true;
        }
        return this._elementMap[this._showElement]();
    }

    _getChatElement() {
        sessionStorage.setItem('state', this._openChat.chatId);
        return html`
            <sn-chat .chat=${this._openChat} .user=${this._checkInfo?.cid} id="chat"></sn-chat>
        `;
    }

    _getSearchElement() {
        sessionStorage.setItem('state', 'search');
        return html`
            <div class="search-results">
                ${
                    this._searchUsersResults.map((e) => html`
                        <sn-search-user .user=${e} .profile=${this._checkInfo?.cid}></sn-search-user>
                    `)
                }
                ${
                    this._searchChannelResults.map((e) => html`
                        <sn-search-channel .channel=${e} .profile=${this._checkInfo?.cid}></sn-search-channel>
                    `)
                }
                ${
                    this._searchChannelResults.length === 0 && this._searchUsersResults.length === 0 ? html`
                        <div class="search-error">
                            <div class="search-error-icon"></div>
                            <span style="margin: auto 0;">Sorry, We couldn't find any results.</span>
                        </div>
                    ` : ``
                }
            </div>
        `;
    }

    _getWelcomeElement() {
        sessionStorage.setItem('state', 'welcome');
        return html`
            <sn-welcome></sn-welcome>
        `;
    }

    _getProfileElement() {
        sessionStorage.setItem('state', 'profile');
        return html`
            <sn-profile .profile=${this._checkInfo?.cid}></sn-profile>
        `;
    }

    _getRegisterElement() {
        sessionStorage.setItem('state', 'register');
        return html`
            <sn-register></sn-register>
        `;
    }

    _getLoginElement() {
        sessionStorage.setItem('state', 'login');
        return html`
            <sn-login
                @execute-login=${(e) => this._executeLogin(e)}
                @register-click=${(e) => this._registerClick(e)}>
            </sn-login>
        `;
    }

    _onLoginSocket(username) {
        const manager = new Manager(Server.SocketsURI);
        const socket = manager.socket("/");
        socket.auth = {
            username: username
        };
        socket.connect();
    }

    _chatBoxClick(e) {
        this._openChat = e.detail;
        this._showElement = FormComponentTypes.Chat;
        this._chat = {};
        this._searchUsersResults = [];
        this._searchChannelResults = [];
        const obj = {
            ref: {
                get: () => this.shadowRoot.getElementById('chat')
            }
        };
        Object.defineProperties(this._chat, obj);
        this._chat.ref?.updateValues(this._openChat);
    }

    _searchUsers(e) {
        this._searchUsersResults = [];
        this._openChat = undefined;
        this._showElement = FormComponentTypes.Search;
        this._searchUsersResults = [...e.detail];
    }

    _searchChannels(e) {
        this._searchChannelResults = [];
        this._openChat = undefined;
        this._showElement = FormComponentTypes.Search;
        this._searchChannelResults = [...e.detail];
    }

    _onProfileClick() {
        this._openChat = undefined;
        this._showElement = FormComponentTypes.Profile;
        this._searchUsersResults = [];
        this._searchChannelResults = [];
    }
}

window.customElements.define(SnForm.is, SnForm);
