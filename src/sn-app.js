import { LitElement, html, css } from 'lit';

class SnApp extends LitElement {
	static get is() {
		return 'sn-app';
	}

	static get styles() {
		return css`
			.wrapper {
				display: flex;
				flex-direction: column;
				flex-grow: 1;
				position: relative;
			}

            .menu {
                position: sticky;
                overflow: hidden;
                top: 0;
                z-index: 10000;
                box-shadow: 0 4px 8px 0 rgb(0 0 0 / 10%);
            }
		`;
	}

	render() {
		return html`
			<div class="wrapper">
                <div class="content">
					<slot></slot>
				</div>
			</div>
		`;
	}

}

window.customElements.define(SnApp.is, SnApp);
