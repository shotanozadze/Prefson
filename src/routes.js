import './sn-app';

export default [
  {
    path: '/',
    component: 'sn-app',
    children: [
      {
        path: '/',
        component: 'sn-form',
        action: () => {
          import('./modules/sn-form');
        }
      }
    ]
  }
];
