export const debounce = (func, timeout) => {
    let timer;
    return (...args) => {
        const next = () => func(...args);
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(next, timeout || 300);
    };
};

export function getCookie(cname) {
    const cookies = document.cookie.split(';');
    for (let i=0; i<cookies.length; i++) {
        const elem = cookies[i];
        const kv = elem.split('=');
        let key = kv[0];
        const val = kv[1];
        if (key[0] === ' ')  key = key.substring(1);
        if (key === cname) return val;
    }
    return null;
}
