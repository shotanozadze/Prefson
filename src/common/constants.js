export const Breakpoints = {
	LARGE: 1920,
	MEDIUM: 1024,
	SMALL: 768,
	M_SMALL: 600,
	X_SMALL: 375
};

export const CookieParamKeys = {
	CX: 'cx',
    CXTOKEN: 'cxToken'
};

export const FormComponentTypes = {
    Chat: 'Chat',
    Search: 'Search',
    Welcome: 'Welcome',
    Profile: 'Profile'
};

export const Server = {
    URI: 'http://139.59.146.173:2017/',
    SocketsURI: 'http://139.59.146.173:3000',
    Login: 'login',
    CheckUser: 'checkuser',
    Register: 'register',
    Chats: 'chats',
    Messages: 'messages',
    SendMessage: 'sendMessage',
    SearchUsers: 'searchUsers',
    SearchChannels: 'searchChannels',
    UpdateProfile: 'updateProfile',
    ConnectUser: 'connectUser',
    CreateChannel: 'createChannel',
    JoinChannel: 'joinChannel',
    LeaveChannel: 'leaveChannel',
    AddFriendToChannel: 'addFriendToChannel'
}

export const WeekDays = [
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat'
];
