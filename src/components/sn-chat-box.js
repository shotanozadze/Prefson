import { LitElement, html, css } from 'lit';
import './sn-chat-user';
import './sn-create-channel';
import { Server } from '../common/constants';
import { Manager } from 'socket.io-client';

class SnChatBox extends LitElement {
	static get is() {
		return 'sn-chat-box';
	}

	static get styles() {
		return css`
            .wrapper {
                transition: all 0.5s ease 0s;
                border-bottom: 1px solid #0f2d37;
                padding-bottom: 16px;
            }

            .header {
                margin: 16px;
                line-height: 20px;
                color: #eaeddd;
                font-family: 'pbold-1';
                user-select: none;
            }

            .not-found {
                color: #71767a;
                font-size: 14px;
                font-family: 'prefson-1';
                user-select: none;
                padding: 0 16px;
            }
		`;
	}

	render() {
		return html`
			<div class="wrapper">
                ${this.isChannel ? html`
                    <div class="header">Channels</div>
                ` : html`
                    <div class="header">Direct messages</div>
                `}
                ${this.isChannel ? this._renderChannels() : this._renderDMs()}
            </div>
		`;
	}

    static get properties() {
        return {
            isChannel: {
                type: Boolean
            },
            userId: {
                type: String
            },
            _chats: {
                type: Array
            },
            _friends: {
                type: Array
            }
        };
    }

    constructor() {
        super();
        this._chats = [];
        this._friends = [];
        this.userId = undefined;
    }

    firstUpdated(_changedProperties) {
        super.firstUpdated(_changedProperties);
        this._loadChats();

        const manager = new Manager(Server.SocketsURI);
        const socket = manager.socket("/");
        if (!this.isChannel) {

            socket.on('connected', (param) => {
                setTimeout(() => {
                    this._updateOnlineUsers(param, false, true);
                }, 100);
            });

            socket.on('disconnected', (param) => {
                setTimeout(() => {
                    this._updateOnlineUsers(param, false, false);
                }, 100);
            });

            socket.on('users', (param) => {
                setTimeout(() => {
                    this._updateOnlineUsers(param, true, false);
                }, 100);
            });
        }
    }

    _loadChats() {
        fetch(Server.URI + Server.Chats, {
            method: "POST",
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(
                {
                    id: this.userId,
                    isChannel: this.isChannel
                }
            )
        })
        .then((blob) => blob.json())
        .then((data) => {
            this._chats = JSON.parse(data).cid;
            this._chats?.map((elem) => {
                const seenTime = new Date(elem.users.find((u) => u.userId === this.userId)?.seen);
                const updateTime = new Date(elem.update);

                if (!elem.isChannel) {
                    let friend = elem.participants[0];
                    if (elem.participants[0]._id === this.userId) {
                        friend = elem.participants[1];
                    }
                    friend.isOnline = false;
                    friend.chatId = elem._id;
                    friend.unread = seenTime < updateTime;
                    this._friends.push(friend);
                } else {
                    elem.unread = seenTime < updateTime;
                }
            });
        });
    }

    _renderChannels() {
        return html`
            ${
                this._chats.map((elem) => html`
                    <sn-chat-user
                        username="@${elem.chatname}"
                        .isChannel=${this.isChannel}
                        .isPublic=${elem.isPublic}
                        chatId=${elem._id}
                        .unread=${elem.unread}
                        @chatbox-click=${this._chatBoxClick.bind(this)}>
                    </sn-chat-user>
                `)
            }
            <sn-create-channel .userId=${this.userId}></sn-create-channel>
        `;
    }

    _renderDMs() {
        return html`
            ${
                this._friends?.map((friend) => {
                    return html`
                        <sn-chat-user
                            fullname="${friend.firstname + ' ' + friend.lastname}"
                            username="@${friend.username}"
                            avatar=${friend.avatar}
                            chatId=${friend.chatId}
                            .unread=${friend.unread}
                            .online=${friend.isOnline}
                            @chatbox-click=${this._chatBoxClick.bind(this)}>
                        </sn-chat-user>
                    `;
                })
            }
            ${
                !this._friends || this._friends.length < 1 ? html`
                    <div class="not-found">You haven't any connections yet.</div>
                ` : ``
            }
        `;
    }

    _updateOnlineUsers(param, isArray, isConnect) {
        if (isArray) {
            param.forEach((e) => {
                this._updateOnlineStatus(e.username, true);
            });
        } else {
            this._updateOnlineStatus(param, isConnect);
        }
    }

    _updateOnlineStatus(user, isConnect) {
        this._friends.filter((e) => e.username === user)
            .map((friend) => {
                friend.isOnline = isConnect;
                this.requestUpdate();
            });
    }

    _chatBoxClick(e) {
        this.dispatchEvent(new CustomEvent('chatbox-click', {
            detail: e.detail
        }));
    }
}

window.customElements.define(SnChatBox.is, SnChatBox);
