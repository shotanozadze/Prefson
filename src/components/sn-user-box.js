import { LitElement, html, css } from 'lit';
import { styleMap } from 'lit/directives/style-map.js';

class SnUserBox extends LitElement {
	static get is() {
		return 'sn-user-box';
	}

	static get styles() {
		return css`
            .wrapper {
                background: #011f26;
                border-bottom: 1px solid #0f2d37;
            }

            .user {
                display: flex;
                height: 64px;
                cursor: pointer;
                transition: all 0.5s ease 0s;
                background: #01181b;
                margin: 16px;
                border-radius: 12px;
                border: 1px solid #0f2d37;
                box-shadow: 0px 0px 3px black;
                position: relative;
            }

            .user:hover {
                background-color: #1b1d21;
            }

            .user-avatar {
                position: relative;
                width: 40px;
                height: 40px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                border-radius: 10px;
                margin-top: 12px;
                margin-left: 16px;
            }

            .user-active {
                position: absolute;
                width: 10px;
                height: 10px;
                background-color: #55d34e;
                right: -4px;
                bottom: -4px;
                border-radius: 50%;
                border: 3px solid #011f26;
            }

            .user-name-wrapper {
                display: flex;
                flex-direction: column;
                margin: 12px 16px;
                user-select: none;
            }

            .user-name {
                font-family: pbold-1;
                color: #eaedde;
                font-size: 17px;
                line-height: 21px;
            }

            .user-username {
                font-family: prefson-1;
                color: #71767a;
                font-size: 16px;
                margin-top: 2px;
            }

            .user-arrow-right {
                width: 24px;
                height: 24px;
                position: absolute;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/rArrow.png);
                right: 16px;
                top: 20px;
            }
		`;
	}

	render() {
        if (!this.avatar) {
            this.avatar = '/assets/images/user.png';
        }
		return html`
			<div class="wrapper">
                <div class="user">
                    <div
                        class="user-avatar"
                        style="${styleMap({'background-image': 'url(' + this.avatar + ')'})}">
                        <div class="user-active"></div>
                    </div>
                    <div class="user-name-wrapper">
                        <div class="user-name">${this.fullname}</div>
                        <div class="user-username">${this.username}</div>
                    </div>
                    <div class="user-arrow-right"></div>
                </div>
            </div>
		`;
	}

    static get properties() {
        return {
            fullname: {
                type: String
            },
            username: {
                type: String
            },
            avatar: {
                type: String
            }
        };
    }

    constructor() {
        super();
    }
}

window.customElements.define(SnUserBox.is, SnUserBox);
