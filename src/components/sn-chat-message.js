import { LitElement, html, css } from 'lit';
import { styleMap } from 'lit/directives/style-map.js';
import CryptoJS from 'crypto-js';
import './sn-time';

class SnChatMessage extends LitElement {
	static get is() {
		return 'sn-chat-message';
	}

	static get styles() {
		return css`
            .wrapper {
                padding: 12px 24px;
                border-top: 1px solid #252727;
                transition: 0.5s;
            }

            .wrapper:hover {
                background: #252727;
            }

            .user-wrapper {
                display: grid;
                grid-template-columns: 56px 1fr;
                transition: 0.5s;
            }

            .user-avatar {
                position: relative;
                width: 40px;
                height: 40px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                border-radius: 10px;
            }

            .user-text-wrapper {
                display: flex;
                flex-direction: column;
            }

            .user-name-wrapper {
                display: flex;
                justify-content: space-between;
            }

            .user-name {
                font-family: pbold-1;
                color: #eaedde;
                font-size: 16px;
                line-height: 20px;
                user-select: none;
            }

            .user-message {
                font-family: prefson-1;
                color: #d1d2d3;
                font-size: 15px;
                line-height: 18px;
                margin-top: 4px;
            }

            @media (max-width: __BREAKPOINT-M-SMALL__) {
                .wrapper {
                    padding: 12px 16px;
                }
            }
		`;
	}

	render() {
		return html`
            ${
                this.messageInfo ? html`
                    <div class="wrapper">
                        <div class="user-wrapper">
                            <div
                                class="user-avatar"
                                style="${styleMap({'background-image': 'url(' + this.messageInfo.avatar + ')'})}">
                            </div>
                            <div class="user-text-wrapper">
                                <div class="user-name-wrapper">
                                    <div class="user-name">${this.messageInfo.fullname}</div>
                                    <sn-time time="${this.messageInfo.time}" id="message-time"></sn-time>
                                </div>
                                <div class="user-message">
                                    ${this._decryptAES(this.messageInfo.message, this.chatId)}
                                </div>
                            </div>
                        </div>
                    </div>
                ` : ``
            }
		`;
	}

    static get properties() {
        return {
            messageInfo: {
                type: Object
            },
            chatId: {
                type: String
            }
        };
    }

    constructor() {
        super();
        this.messageInfo = undefined;
        this.chatId = undefined;
    }

    _decryptAES (encryptedBase64, key) {
        const decrypted = CryptoJS.AES.decrypt(encryptedBase64, key);
        if (decrypted) {
            try {
                const str = decrypted.toString(CryptoJS.enc.Utf8);
                if (str.length > 0) {
                    return str;
                } else {
                    return 'err';
                } 
            } catch (e) {
                return 'err';
            }
        }
        return 'err';
    };

    updated(_changedProperties) {
        const time = {};
        const obj = {
            ref: {
                get: () => this.shadowRoot.getElementById('message-time')
            }
        };
        Object.defineProperties(time, obj);
        time.ref.updateValues(this.messageInfo.time);
    }
}

window.customElements.define(SnChatMessage.is, SnChatMessage);
