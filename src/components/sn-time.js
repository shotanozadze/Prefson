import { LitElement, html, css } from 'lit';
import { WeekDays} from '../common/constants';

class SnTime extends LitElement {
	static get is() {
		return 'sn-time';
	}

	static get styles() {
		return css`
            .wrapper {
                font-family: prefson-1;
                color: #ababad;
                font-size: 12px;
                line-height: 21px;
                user-select: none;
            }
		`;
	}

	render() {
		return html`
            <div class="wrapper">
                ${this._showTime}
            </div>
		`;
	}

    static get properties() {
        return {
            time: {
                type: String
            },
            _showTime: {
                type: String
            }
        };
    }

    constructor() {
        super();
        this.time = '';
        this._showTime = '';
    }

    _getTime(dt) {
        const date = dt.getDate();
        const month = dt.getMonth()+1;
        const day = WeekDays[dt.getDay()];
        const year = dt.getFullYear();
        const hour = dt.getHours();
        const min = dt.getMinutes();
        return {
            date, month, day, year, hour, min
        };
    }

    _displayDate(messageTime, currentTime) {        
        if (currentTime.year - messageTime.year > 0) return this._isOldYear(messageTime);
        const daysAgo = currentTime.date - messageTime.date;
        if (currentTime.month - messageTime.month > 0 || daysAgo > 6) return this._isOldMonth(messageTime);
        if (daysAgo > 0 && daysAgo < 7) return messageTime.day;
        return '';
    }

    _displayTime(time) {
        const tm = this._toString(time.hour) + ':'
            + this._toString(time.min);
        return tm;
    }

    _isOldYear(time) {
        const dt = this._toString(time.date) + '/'
            + this._toString(time.month) + '/' + time.year;
        return dt;
    }

    _isOldMonth(time) {
        const dt = this._toString(time.date) + '/' + this._toString(time.month);
        return dt;
    }

    _toString(param) {
        if (param < 10) {
            return '0' + String(param);
        }
        return String(param);
    }

    updateValues(time) {
        this.time = time;
        this._showTime = '';
        const messageTime = this._getTime(new Date(this.time));
        const currentTime = this._getTime(new Date());
        this._showTime = this._displayDate(messageTime, currentTime) +
            ' ' + this._displayTime(messageTime);
    }
}

window.customElements.define(SnTime.is, SnTime);
