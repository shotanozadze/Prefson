import { LitElement, html, css } from 'lit';
import { classMap } from 'lit/directives/class-map.js';
import { styleMap } from 'lit/directives/style-map.js';
import { Server } from '../common/constants';
import { Manager } from 'socket.io-client';

class SnChatUser extends LitElement {
	static get is() {
		return 'sn-chat-user';
	}

	static get styles() {
		return css`
            .wrapper {
                position: relative;
                display: flex;
                min-height: 40px;
                cursor: pointer;
                transition: all 0.3s ease 0s;
                background: rgb(1, 24, 28);
                margin: 8px 16px;
                border-radius: 8px;
                border: 1px solid rgb(11, 38, 46);
                box-shadow: black 0px 0px 3px;
            }

            .wrapper:hover {
                background-color: #1b1d21;
            }

            .user_avatar {
                width: 24px;
                height: 24px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: cover;
                border-radius: 4px;
                margin: auto 0 auto 12px;
                position: relative;
            }

            .channel_icon {
                width: 20px;
                height: 20px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                border-radius: 4px;
                margin: auto 0 auto 12px;
                position: relative;
            }

            .user_active {
                position: absolute;
                width: 8px;
                height: 8px;
                background-color: #55d34e;
                right: -4px;
                bottom: -4px;
                border-radius: 50%;
                border: 2.5px solid #011f26;
            }

            .user_inactive {
                position: absolute;
                width: 8px;
                height: 8px;
                background-color: #b1bab1;
                right: -4px;
                bottom: -4px;
                border-radius: 50%;
                border: 2.5px solid #011f26;
            }

            .user-name-wrapper {
                display: flex;
                flex-direction: row;
                margin: 8px 12px;
                user-select: none;
            }

            .user-name {
                font-family: pbold-1;
                color: #eaedde;
                font-size: 16px;
                margin: auto;
                padding-right: 12px;
            }

            .user-username {
                font-family: prefson-1;
                color: #71767a;
                font-size: 14px;
                margin: auto;
            }

            .unread {
                position: absolute;
                width: 10px;
                height: 10px;
                background-color: #e41e3f;
                border-radius: 50%;
                right: 8px;
                top: 0;
                bottom: 0;
                margin: auto 0;
            }
		`;
	}

	render() {
        if (this.isChannel && this.isPublic) {
            this.avatar = '/assets/images/public.png';
        }

        if (this.isChannel && !this.isPublic) {
            this.avatar = '/assets/images/private.png';
        }

        if (!this.isChannel && !this.avatar) {
            this.avatar = '/assets/images/user.png';
        }

		return html`
			<div class="wrapper" @click=${this._onChatClick.bind(this)}>
                <div
                    class="${classMap({
                        channel_icon: this.isChannel,
                        user_avatar: !this.isChannel
                    })}"
                    style="${styleMap({'background-image': 'url(' + this.avatar + ')'})}">
                    ${
                        !this.isChannel ? html`
                            <div class="${classMap({
                                    user_active: this.online,
                                    user_inactive: !this.online
                                })}">
                            </div>
                        ` : ``
                    }
                </div>
                <div class="user-name-wrapper">
                    ${
                        !this.isChannel ? html`
                            <div class="user-name">${this.fullname}</div>
                        ` : ``
                    }
                    <div class="user-username">${this.username}</div>
                </div>
                
                ${
                    this.unread ? html`
                        <div class="unread"></div>    
                    ` : ``
                }
            </div>
        `;
	}

    static get properties() {
        return {
            fullname: {
                type: String
            },
            username: {
                type: String
            },
            avatar: {
                type: String
            },
            isChannel: {
                type: Boolean
            },
            isPublic: {
                type: Boolean
            },
            online: {
                type: Boolean
            },
            chatId: {
                type: String
            },
            unread: {
                type: Boolean
            }
        };
    }

    constructor() {
        super();
        this.fullname = '';
        this.username = '';
        this.avatar = '';
        this.isChannel = false;
        this.isPublic = false;
        this.online = false;
        this.chatId = '';
        this.unread = false;
    }

    firstUpdated(_changedProperties) {
        super.firstUpdated(_changedProperties);
        const manager = new Manager(Server.SocketsURI);
        const socket = manager.socket("/");
        socket.on('get message_' + this.chatId, (param) => {
            if (sessionStorage.getItem('state') !== this.chatId) {
                this.unread = true;
                const audio = new Audio('assets/sounds/message.mp3');
                audio.play();
            }
        });
    }

    _onChatClick(e) {
        this.unread = false;
        this.dispatchEvent(new CustomEvent('chatbox-click', {
            detail: {
                chatId: this.chatId,
                username: this.username,
                fullname: this.fullname,
                avatar: this.avatar,
                isChannel: this.isChannel
            }
        })); 
    }
}

window.customElements.define(SnChatUser.is, SnChatUser);
