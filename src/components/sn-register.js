import { LitElement, html, css } from 'lit';
import sha256 from 'crypto-js/sha256';
import { Server } from '../common/constants';

class SnRegister extends LitElement {
	static get is() {
		return 'sn-register';
	}

	static get styles() {
		return css`
			.wrapper {
				margin: 70px auto;
                width: 640px;
                background: #fcfcfc;
                border-radius: 12px;
                box-shadow: rgb(0 0 0 / 10%) 0px 4px 8px 0px;
			}

            .header {
                font-family: prefson-1;
                color: rgb(60, 60, 60);
                font-size: 18px;
                font-weight: bold;
                padding: 32px 48px;
                border-bottom: 1px solid #e8e8e8;
                cursor: default;
            }

            .container {
                display: flex;
                flex-direction: row;
                padding: 24px 48px 0 48px;
            }

            .container-register {
                width: 65%;
            }

            .container-logo {
                width: 100px;
                height: 100px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/swan.png);
                margin: 24px auto;
            }

            .register-field {
                width: calc(100% - 32px);
                height: 20px;
                padding: 12px 16px;
                font-family: prefson-1;
                font-size: 14px;
                border-radius: 8px;
                border: 1px solid rgb(232, 232, 232);
                background: #fcfcfc;
                outline: none;
                color: #141414;
                margin-bottom: 16px;
            }

            .register-field::placeholder {
                color: #808080;
            }
            
            .register-buttons {
                padding: 0 48px 48px;
            }

            .submit-butt {
                background-color: #f6c245;
                color: #fcfcfc;
                padding: 14px;
                border: none;
                border-radius: 8px;
                cursor: pointer;
                font-family: prefson-1;
                font-size: 20px;
                font-weight: bold;
                box-shadow: rgb(0 0 0 / 10%) 0px 4px 8px 0px;
                text-shadow: rgb(0 0 0 / 10%) 2px 2px;
                transition: 0.5s;
                text-align: center;
                cursor: pointer;
            }

            .submit-butt:hover {
                background-color: #f9cc62;
            }

            .register-error {
                display: flex;
                background: #fdeeee;
                padding: 12px 16px;
                margin-bottom: 16px;
                font-family: prefson-1;
                font-size: 12px;
                border-radius: 4px;
                border: 1px solid #e22820;
                color: #141111;
            }

            .register-error-icon {
                width: 20px;
                height: 20px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/error.png);
                margin-right: 12px;
            }

            .register-success-main {
                display: flex;
                flex-direction: column;
                padding-bottom: 24px;
            }

            .register-success-main-icon {
                min-width: 128px;
                min-height: 128px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/success.png);
            }

            .register-success-main-text {
                margin-top: 12px;
                font-family: prefson-2;
                font-weight: bolder;
                font-size: 30px;
                text-align: center;
                color: #07a06e;
            }

            .register-success {
                display: flex;
                background: #eaf8f0;
                padding: 12px 16px;
                margin-bottom: 16px;
                font-family: prefson-1;
                font-size: 13px;
                line-height: 17px;
                border-radius: 4px;
                border: 1px solid #38af5f;
                color: #141111;
            }

            .register-success-icon {
                min-width: 24px;
                min-height: 24px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/success.png);
                margin-right: 12px;
            }

            @media (max-width: __BREAKPOINT-M-SMALL__) {
                .wrapper {
                    width: 100%;
                    border-radius: 0;
                }
            }

            @media (max-width: __BREAKPOINT-X-SMALL__) {
                .wrapper {
                    margin: 0px auto;
                    height: calc(100vh - 80px);
                }

                .header {
                    font-size: 16px;
                    padding: 24px 48px;
                }

                .container-logo {
                    display: none;
                }

                .container-register {
                    width: 100%;
                }

                .register-field {
                    font-size: 13px;
                }

                .submit-butt {
                    font-size: 18px;
                }

                .register-error {
                    margin-bottom: 16px;
                }
            }
		`;
	}

	render() {
		return html`
            <div class="wrapper">
                <div class="header">Create your Prefson account</div>
                <div class="container">
                    <div class="container-register">
                        ${
                            this._registerSuccess ? html`
                                <div class="register-success-main">
                                    <div class="register-success-main-icon"></div>
                                    <div class="register-success-main-text">SUCCESS!</div>
                                </div>

                                <div class="register-success">
                                    <div class="register-success-icon"></div>
                                    <div style="margin: auto 0;">${this._registerSuccess}</div>
                                </div>
                            ` : html`
                                <input type="text" placeholder="Username" class="register-field" id="sn-username" required @keypress=${this._onRegisterPress.bind(this)}>
                                <input type="text" placeholder="Email address" class="register-field" id="sn-mail" required @keypress=${this._onRegisterPress.bind(this)}>
                                <div style="display: flex;">
                                    <input type="text" placeholder="First name" class="register-field" style="margin-right: 12px" id="sn-fname" required @keypress=${this._onRegisterPress.bind(this)}>
                                    <input type="text" placeholder="Last name" class="register-field" id="sn-lname" required @keypress=${this._onRegisterPress.bind(this)}>
                                </div>
                                <input type="password" placeholder="Password" class="register-field" id="sn-password" required @keypress=${this._onRegisterPress.bind(this)}>
                                ${
                                    this._registerError && !this._validError ? html`
                                        <div class="register-error">
                                            <div class="register-error-icon"></div>
                                            <span style="margin: auto 0;">${this._registerError}</span>
                                        </div>   
                                    ` : ``
                                }

                                ${
                                    this._validError ? html`
                                        <div class="register-error">
                                            <div class="register-error-icon"></div>
                                            <span style="margin: auto 0;">${this._validError}</span>
                                        </div>   
                                    ` : ``
                                }
                            `
                        }
                    </div>
                    <div class="container-logo"></div>
                </div>

                <div class="register-buttons">
                    ${
                        this._registerSuccess ? html`
                            <div class="submit-butt" @click=${() => location.reload()}>Sign In Now</div>
                        ` : html`
                            <div class="submit-butt" @click=${this._onRegisterClick.bind(this)}>Sign Up</div>
                        `
                    }
                </div>
            </div>
		`;
	}

    static get properties() {
        return {
            _registerError: {
                type: String
            },
            _registerSuccess: {
                type: String
            },
            _validError: {
                type: String
            }
        }
    }

    constructor() {
        super();
        this._registerError = undefined;
        this._registerSuccess = undefined;
        this._validError = undefined;
    }

    firstUpdated(_changedProperties) {
        super.firstUpdated(_changedProperties);
    }

    _onRegisterClick(e) {
        const mail = this.shadowRoot.getElementById('sn-mail').value;
        const username = this.shadowRoot.getElementById('sn-username').value;
        const pass = this.shadowRoot.getElementById('sn-password').value;
        const fname = this.shadowRoot.getElementById('sn-fname').value;
        const lname = this.shadowRoot.getElementById('sn-lname').value;
        if (!username || !pass || !mail || !fname || !lname) {
            this._validError = 'Please, fill out all required fields.';
            return;
        }
        if (!this._usernameValid(username.toLowerCase())) {
            this._validError = 'Invalid username. Please, use only letters(a-z), numbers and periods.';
            return;
        }
        if (!this._mailValid(mail)) {
            this._validError = 'Email address invalid.';
            return;
        }
        if (pass.length < 6) {
            this._validError = 'Password must be 6 or more characters in length.';
            return;
        }
        fetch(Server.URI + Server.Register, {
            method: "POST",
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(
                {
                    username: username.toLowerCase(),
                    password: sha256(pass).toString(),
                    mail: mail.toLowerCase(),
                    firstname: fname,
                    lastname: lname,
                    avatar: '',
                    connections: []
                }
            )
        })
        .then((blob) => blob.json())
        .then((data) => {
            const res = JSON.parse(data);
            if (res.success) {
                this._registerSuccess = res.message;
            } else {
                this._validError = undefined;
                this._registerError = res.message;
            }
        });
    }

    _onRegisterPress(e) {
        if (e.key === 'Enter') {
            this._onRegisterClick();
        }
    }

    _usernameValid(username) {
        for (let i=0; i<username.length; i++) {
            const isLower = username[i]>= 'a' && username[i]<='z';
            const isSchar = username[i] === '.' || username[i] === '-' || username[i] === '_';
            const isNum = username[i] >= '0' && username[i] <= '9';
            if (!isLower && !isNum && !isSchar) {
                return false;
            }
        }
        return true;
    }

    _mailValid(mail) {
        if (mail.includes('@') && mail.includes('.')) {
            return true;
        }
        return false;
    }
}

window.customElements.define(SnRegister.is, SnRegister);
