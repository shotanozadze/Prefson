import { LitElement, html, css } from 'lit';

class SnWelcome extends LitElement {
	static get is() {
		return 'sn-welcome';
	}

	static get styles() {
		return css`
            .wrapper {
                position: relative;
				margin: 150px auto 30px;
                display: flex;
                justify-content: center;
            }

            .box {
                display: flex;
                flex-direction: column;
            }

            .prefson-wrapper {
                display: flex;
            }

            .prefson-logo {
                width: 80px;
                height: 80px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/user.png);
                border-radius: 12px;
            }

            .prefson-title-wrapper {
                display: flex;
                flex-direction: column;
                margin-left: 24px;
                user-select: none;
            }

            .prefson-title {
                font-family: prefson-1;
                color: #f4dd7f;
                font-size: 30px;
                line-height: 42px;
                font-weight: bold;
            }

            .prefson-info {
                font-family: prefson-2;
                color: #ebedde;
                font-size: 18px;
                line-height: 30px;
                font-weight: bold;
            }

            .check-list {
                display: flex;
                flex-direction: column;
                margin-top: 24px;
            }

            .check-wrapper {
                display: flex;
                margin-top: 12px;
            }

            .check-icon-wrapper {
                width: 24px;
                height: 24px;
                background: #2e4141;
                border-radius: 50%;
                box-shadow: black 0px 0px 3px;
            }

            .check-icon {
                width: 18px;
                height: 18px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/check.png);
                border-radius: 50%;
                margin: 4px;
            }

            .check-text {
                font-family: prefson-1;
                color: #ebedde;
                font-size: 14px;
                line-height: 24px;
                font-weight: bold;
                margin-left: 8px;
            }
		`;
	}

	render() {
		return html`
            <div class="wrapper">
                <div class="box">
                    <div class="prefson-wrapper">
                        <div class="prefson-logo"></div>
                        <div class="prefson-title-wrapper">
                            <div class="prefson-title">Prefson</div>
                            <div class="prefson-info">Realtime Chat App</div>
                        </div>
                    </div>
    
                    <div class="check-list">
                        <div class="check-wrapper">
                            <div class="check-icon-wrapper">
                                <div class="check-icon"></div>
                            </div>
                            <div class="check-text">Communicate with your friends.</div>
                        </div>
                        <div class="check-wrapper">
                            <div class="check-icon-wrapper">
                                <div class="check-icon"></div>
                            </div>
                            <div class="check-text">Create public and private channels.</div>
                        </div>
                        <div class="check-wrapper">
                            <div class="check-icon-wrapper">
                                <div class="check-icon"></div>
                            </div>
                            <div class="check-text">Realtime communication.</div>
                        </div>
                        <div class="check-wrapper">
                            <div class="check-icon-wrapper">
                                <div class="check-icon"></div>
                            </div>
                            <div class="check-text">Find friends.</div>
                        </div>
                        <div class="check-wrapper">
                            <div class="check-icon-wrapper">
                                <div class="check-icon"></div>
                            </div>
                            <div class="check-text">Find public channels.</div>
                        </div>
                    </div>
                </div>
            </div>
		`;
	}
}

window.customElements.define(SnWelcome.is, SnWelcome);
