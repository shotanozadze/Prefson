import { LitElement, html, css } from 'lit';
import { classMap } from 'lit/directives/class-map.js';
import { Server } from '../common/constants';

class SnCreateChannel extends LitElement {
	static get is() {
		return 'sn-create-channel';
	}

	static get styles() {
		return css`
			.wrapper {
                display: flex;
                flex-direction: column;
                transition: all 0.3s ease 0s;
                background: rgb(1, 24, 28);
                margin: 8px 16px;
                border-radius: 8px;
                border: 1px solid #0b262f;
                box-shadow: black 0px 0px 3px;
            }

            .box-wrapper {
                display: flex;
                padding: 4px 0;
                cursor: pointer;
            }

            .channel_icon {
                width: 20px;
                height: 20px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/add.png);
                border-radius: 4px;
                margin: auto 0 auto 12px;
                position: relative;
            }

            .channel-label-wrapper {
                display: flex;
                flex-direction: row;
                margin: 8px 12px;
                user-select: none;
            }

            .channel-label {
                font-family: pbold-1;
                color: #d0d0d0;
                font-size: 16px;
                margin: auto;
                padding-right: 12px;
            }

            .create-channel-wrapper {
                width: 100%;
                height: 0;
                transition: 0.5s;
                display: flex;
                flex-direction: column;
            }

            .create_active {
                border-top: 1px solid #0b262f;
                height: 170px;
            }

            .public-checkbox {
                color: #faead7;
                font-family: pbold-1;
                font-size: 14px;
                user-select: none;
                padding: 12px;
            }

            .channel-field {
                padding: 8px 16px;
                font-family: prefson-1;
                font-size: 15px;
                border-radius: 4px;
                border: 1px solid rgb(70, 84, 87);
                background: rgb(47, 64, 65);
                outline: none;
                color: rgb(201, 206, 194);
                height: 16px;
                margin: 0 16px;
            }

            .create-butt {
                margin: 12px 16px;
                color: #faead7;
                font-family: pbold-1;
                font-size: 15px;
                user-select: none;
                padding: 10px;
                background: #011f26;
                text-align: center;
                border-radius: 8px;
                cursor: pointer;
                box-shadow: 0px 0px 2px #ffffff42;
            }

            .create-error {
                display: flex;
                background: rgb(253, 238, 238);
                padding: 5px;
                margin: -3px 16px;
                font-family: prefson-1;
                font-size: 12px;
                border-radius: 20px;
                color: #141111;
                border: 1px solid red;
            }

            .create-error-icon {
                width: 14px;
                height: 14px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/error.png);
                margin: 0 8px;
            }
		`;
	}

	render() {
		return html`
            <div class="wrapper">
                <div class="box-wrapper" @click=${this._toggleDropDown.bind(this)}>
                    <div class="channel_icon"></div>
                    <div class="channel-label-wrapper">
                        <div class="channel-label">Create Channel</div>
                    </div>
                </div>
                <div class="create-channel-wrapper
                    ${classMap({ create_active: this._dropDown})}">
                    ${this._dropDown ? this._renderCreate() : ``}
                </div>
            </div>
		`;
	}

    static get properties() {
        return {
            userId: {
                type: String
            },
            _dropDown: {
                type: Boolean
            },
            _invalid: {
                type: Boolean
            }
        }
    }

    constructor() {
        super();
        this.userId = undefined;
        this._dropDown = false;
        this._invalid = false;
    }

    firstUpdated(_changedProperties) {
        super.firstUpdated(_changedProperties);
    }

    _renderCreate() {
        return html`
            <div class="public-checkbox">
                <input type="checkbox" id="public-channel">
                <span
                    style="margin: 4px"
                    @click=${this._onPublicClick.bind(this)}>
                    Public Channel
                </span>
            </div>
            <input
                type="text"
                placeholder="Channel Name"
                class="channel-field"
                id="channel-name"
                @input=${() => this._invalid = false}
            >
            <div class="create-butt" @click=${this._createChannel.bind(this)}>Create</div>
            ${
                this._invalid ? html`
                     <div class="create-error">
                        <div class="create-error-icon"></div>
                        <span>Channel Name isn't valid.</span>
                    </div>
                ` : ``
            }
        `;
    }

    _toggleDropDown() {
        if (this._dropDown) {
            this._dropDown = false;
        } else {
            this._dropDown = true;
        }
    }

    _onPublicClick() {
        const checkbox = this.shadowRoot.getElementById('public-channel');
        if (checkbox?.checked) {
            checkbox.checked = false;
        } else {
            checkbox.checked = true;
        }
    }

    _createChannel() {
        const isPublic = this.shadowRoot.getElementById('public-channel').checked;
        const name = this.shadowRoot.getElementById('channel-name').value.toLowerCase();
        if (name.length === 0) return;
        fetch(Server.URI + Server.CreateChannel, {
            method: "POST",
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(
                {
                    chatname: name,
                    isChannel: true,
                    isPublic: isPublic,
                    userId: this.userId
                }
            )
        })
        .then((blob) => blob.json())
        .then((data) => {
            const res = JSON.parse(data);
            if (res.success) {
                location.reload();
            } else {
                this._invalid = true;
            }
        });
    }
}

window.customElements.define(SnCreateChannel.is, SnCreateChannel);
