import { LitElement, html, css } from 'lit';
import sha256 from 'crypto-js/sha256';
import { Server } from '../common/constants';

class SnProfile extends LitElement {
	static get is() {
		return 'sn-profile';
	}

	static get styles() {
		return css`
			.wrapper {
				margin: 24px auto;
                width: 640px;
                background: #fcfcfc;
                border-radius: 12px;
                box-shadow: rgb(0 0 0 / 10%) 0px 4px 8px 0px;
			}

            .header {
                font-family: prefson-1;
                color: rgb(60, 60, 60);
                font-size: 18px;
                font-weight: bold;
                padding: 32px 48px;
                border-bottom: 1px solid #e8e8e8;
                cursor: default;
            }

            .container {
                display: flex;
                flex-direction: row;
                padding: 24px 48px 0 48px;
            }

            .container-register {
                width: 65%;
            }

            .container-logo {
                width: 100px;
                height: 100px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/swan.png);
                margin: 24px auto;
            }

            .profile-field {
                width: calc(100% - 32px);
                height: 20px;
                padding: 12px 16px;
                font-family: prefson-1;
                font-size: 14px;
                border-radius: 8px;
                border: 1px solid rgb(232, 232, 232);
                background: #fcfcfc;
                outline: none;
                color: #141414;
                margin-bottom: 16px;
            }

            .profile-field::placeholder {
                color: #808080;
            }

            .readonly-field {
                width: calc(100% - 32px);
                height: 20px;
                padding: 12px 16px;
                font-family: prefson-1;
                font-size: 14px;
                border-radius: 8px;
                border: 1px solid rgb(232, 232, 232);
                background: #fcfcfc;
                outline: none;
                margin-bottom: 16px;
                color: #808080;
            }
            
            .register-buttons {
                padding: 0 48px 48px;
            }

            .submit-butt {
                background-color: #f6c245;
                color: #fcfcfc;
                padding: 14px;
                border: none;
                border-radius: 8px;
                cursor: pointer;
                font-family: prefson-1;
                font-size: 20px;
                font-weight: bold;
                box-shadow: rgb(0 0 0 / 10%) 0px 4px 8px 0px;
                text-shadow: rgb(0 0 0 / 10%) 2px 2px;
                transition: 0.5s;
                text-align: center;
                cursor: pointer;
            }

            .submit-butt:hover {
                background-color: #f9cc62;
            }

            .profile-error {
                display: flex;
                background: #fdeeee;
                padding: 12px 16px;
                margin-bottom: 16px;
                font-family: prefson-1;
                font-size: 12px;
                border-radius: 4px;
                border: 1px solid #e22820;
                color: #141111;
            }

            .profile-error-icon {
                width: 20px;
                height: 20px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/error.png);
                margin-right: 12px;
            }

            .profile-success-main {
                display: flex;
                flex-direction: column;
                padding-bottom: 24px;
            }

            .profile-success-main-icon {
                min-width: 128px;
                min-height: 128px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/success.png);
            }

            .profile-success-main-text {
                margin-top: 12px;
                font-family: prefson-2;
                font-weight: bolder;
                font-size: 30px;
                text-align: center;
                color: #07a06e;
            }

            .profile-success {
                display: flex;
                background: #eaf8f0;
                padding: 12px 16px;
                margin-bottom: 16px;
                font-family: prefson-1;
                font-size: 13px;
                line-height: 17px;
                border-radius: 4px;
                border: 1px solid #38af5f;
                color: #141111;
            }

            .profile-success-icon {
                min-width: 24px;
                min-height: 24px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/success.png);
                margin-right: 12px;
            }

            @media (max-width: __BREAKPOINT-M-SMALL__) {
                .wrapper {
                    width: 100%;
                    border-radius: 0;
                }
            }

            @media (max-width: __BREAKPOINT-X-SMALL__) {
                .wrapper {
                    margin: 0px auto;
                    height: calc(100vh - 80px);
                }

                .header {
                    font-size: 16px;
                    padding: 24px 48px;
                }

                .container-logo {
                    display: none;
                }

                .container-register {
                    width: 100%;
                }

                .profile-field {
                    font-size: 13px;
                }

                .submit-butt {
                    font-size: 18px;
                }

                .profile-error {
                    margin-bottom: 16px;
                }
            }
		`;
	}

	render() {
		return html`
            <div class="wrapper">
                <div class="header">Personal Information</div>
                <div class="container">
                    <div class="container-register">
                        ${
                            this._profileSuccess ? html`
                                <div class="profile-success-main">
                                    <div class="profile-success-main-icon"></div>
                                    <div class="profile-success-main-text">SUCCESS!</div>
                                </div>

                                <div class="profile-success">
                                    <div class="profile-success-icon"></div>
                                    <div style="margin: auto 0;">${this._profileSuccess}</div>
                                </div>
                            ` : html`
                                <input type="text" placeholder="Username" class="readonly-field" id="sn-username" value="${this.profile.username}" readonly>
                                <input type="text" placeholder="Email address" class="readonly-field" id="sn-mail" value=${this.profile.mail} readonly>
                                <input type="text" placeholder="First name" class="profile-field" id="sn-fname" value=${this.profile.firstname}>
                                <input type="text" placeholder="Last name" class="profile-field" id="sn-lname" value=${this.profile.lastname}>
                                <input type="text" placeholder="Avatar" class="profile-field" id="sn-avatar" value=${this.profile.avatar}>
                                <input type="password" placeholder="New Password" class="profile-field" id="sn-password">
                                ${
                                    this._profileError && !this._validError ? html`
                                        <div class="profile-error">
                                            <div class="profile-error-icon"></div>
                                            <span style="margin: auto 0;">${this._profileError}</span>
                                        </div>
                                    ` : ``
                                }
                            `
                        }
                    </div>
                    <div class="container-logo"></div>
                </div>

                <div class="register-buttons">
                    ${
                        !this._profileSuccess ? html`
                            <div class="submit-butt" @click=${this._onUpdateClick.bind(this)}>Update</div>
                        ` : html`
                            <div class="submit-butt" @click=${() => location.reload()}>OK</div>
                        `
                    }
                </div>
            </div>
		`;
	}

    static get properties() {
        return {
            profile: {
                type: Object
            },
            _profileError: {
                type: String
            },
            _profileSuccess: {
                type: String
            }
        }
    }

    _onUpdateClick(e) {
        const fname = this.shadowRoot.getElementById('sn-fname').value;
        const lname = this.shadowRoot.getElementById('sn-lname').value;
        const avatar = this.shadowRoot.getElementById('sn-avatar').value;
        const password = this.shadowRoot.getElementById('sn-password').value;
        let passHash = '';

        if (password.length > 0 && password.length < 6) {
            this._profileError = 'Password must be 6 or more characters in length.';
            return;
        }

        if (fname.length < 1 || lname.length < 1) {
            this._profileError = 'Firstname and Lastname fields should be filled.';
            return;
        }

        if (password.length >= 6) {
            passHash = sha256(password).toString();
        }

        fetch(Server.URI + Server.UpdateProfile, {
            method: "POST",
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(
                {
                    username: this.profile.username,
                    firstname: fname,
                    lastname: lname,
                    avatar: avatar,
                    password: passHash
                }
            )
        })
        .then((blob) => blob.json())
        .then((data) => {
            const res = JSON.parse(data);
            if (res.acknowledged) {
                this._profileSuccess = 'Profile updated successfully!';
            }
        });
    }
}

window.customElements.define(SnProfile.is, SnProfile);
