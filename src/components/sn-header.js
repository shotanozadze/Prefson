import { LitElement, html, css } from 'lit';
import { CookieParamKeys, Server } from '../common/constants';

class SnHeader extends LitElement {
	static get is() {
		return 'sn-header';
	}

	static get styles() {
		return css`
			.wrapper {
                width: 100%;
                height: 64px;
                background-color: #01161a;
                display: flex;
                border-bottom: 1px solid #1a3339;
                justify-content: space-between;
			}

            .header-logo {
                width: 48px;
                height: 48px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/swan.png);
                margin-top: 8px;
                cursor: pointer;
                margin-left: 32px;
            }

            .search-wrapper {
                display: flex;
                margin: 12px 24px;
                width: 50%;
                position: relative;
            }

            .search-field {
                padding: 8px 16px;
                font-family: prefson-1;
                font-size: 15px;
                border-radius: 30px;
                border: 1px solid #465457;
                background: #2f4041;
                outline: none;
                color: #c9cec2;
                height: 22px;
                width: 100%;
            }

            .search-icon {
                width: 32px;
                height: 32px;
                position: absolute;
                right: 4px;
                top: 4px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/search.png);
                background-color: #011f26;
                border-radius: 30px;
                cursor: pointer;
            }

            .logout {
                width: 20px;
                height: 20px;
                margin-top: 22px;
                cursor: pointer;
                margin-right: 24px;
            }

            .logout img {
                width: 100%;
                height: 100%;
            }

            @media (max-width: __BREAKPOINT-M-SMALL__) {
                .header-logo {
                    margin-left: 12px;
                }

                .logout {
                    margin-right: 12px;
                }

                .search-wrapper { 
                    width: 65%;
                }
            }
		`;
	}

	render() {
		return html`
			<div class="wrapper">
                <div class="header-logo" @click=${() => location.reload()}></div>
                ${
                    this.user?.success ? html`
                        <div class="search-wrapper">
                            <input type="text" placeholder="Search Prefson" class="search-field" id="sn-search" @keypress=${this._onSearchPress.bind(this)}>
                            <div class="search-icon" @click=${this._onSearchClick.bind(this)}></div>
                        </div>
                        <div
                            class="logout"
                            @click=${this._executeLogOut}>
                            <img src="/assets/images/logout.png">
                        </div>
                    ` : ``
                }
            </div>
		`;
	}

    static get properties() {
        return {
            menu: {
                type: Array
            },
            user: {
                type: Object
            }
        };
    }

    constructor() {
        super();
        this.menu = this._menuItems;
    }

    get _menuItems() {
        return [
            {
                title: 'მთავარი',
                href: '/'
            }
        ];
    }

    _executeLogOut() {
        document.cookie = CookieParamKeys.CX + '=';
        document.cookie = CookieParamKeys.CXTOKEN + '=';
        location.reload();
    }

    _onSearchClick(e) {
        const query = this.shadowRoot.getElementById('sn-search').value;
        if (query.length === 0) return;
        this.shadowRoot.getElementById('sn-search').value = '';
        this._fetchSearchResults(query, Server.SearchUsers);
        this._fetchSearchResults(query, Server.SearchChannels);
    }

    _onSearchPress(e) {
        if (e.key === 'Enter') {
            this._onSearchClick(e);
        }
    }

    _fetchSearchResults(query, collection) {
        fetch(Server.URI + collection, {
            method: "POST",
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(
                {
                    query: query
                }
            )
        })
        .then((blob) => blob.json())
        .then((data) => {
            const results = JSON.parse(data);
            this.dispatchEvent(new CustomEvent(collection, {
                detail: results
            }));
        });
    }
}

window.customElements.define(SnHeader.is, SnHeader);
