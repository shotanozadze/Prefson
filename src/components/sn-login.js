import { LitElement, html, css } from 'lit';
import sha256 from 'crypto-js/sha256';
import { Server } from '../common/constants';
import { Manager } from 'socket.io-client';

class SnLogin extends LitElement {
	static get is() {
		return 'sn-login';
	}

	static get styles() {
		return css`
			.wrapper {
				margin: 70px auto;
                width: 640px;
                background: #fcfcfc;
                border-radius: 12px;
                box-shadow: rgb(255 255 255 / 38%) 0px 4px 8px 0px;
			}

            .header {
                font-family: prefson-1;
                color: rgb(60, 60, 60);
                font-size: 18px;
                font-weight: bold;
                padding: 32px 48px;
                border-bottom: 1px solid #e8e8e8;
                cursor: default;
            }

            .container {
                display: flex;
                flex-direction: row;
                padding: 48px 48px 0 48px;
            }

            .container-login {
                width: 65%;
            }

            .container-logo {
                width: 100px;
                height: 100px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/swan.png);
                margin: 24px auto;
            }

            .login-field {
                width: calc(100% - 32px);
                height: 20px;
                padding: 16px;
                font-family: prefson-1;
                font-size: 14px;
                border-radius: 8px;
                border: 1px solid rgb(232, 232, 232);
                background: #fcfcfc;
                outline: none;
                color: #141414;
                margin-bottom: 24px;
            }

            .login-field::placeholder {
                color: #808080;
            }
            
            .login-buttons {
                padding: 24px 48px 64px 48px;
            }

            .submit-butt {
                background-color: #f6c245;
                color: #fcfcfc;
                padding: 14px;
                border: none;
                border-radius: 8px;
                cursor: pointer;
                font-family: prefson-1;
                font-size: 20px;
                font-weight: bold;
                box-shadow: rgb(0 0 0 / 10%) 0px 4px 8px 0px;
                text-shadow: rgb(0 0 0 / 10%) 2px 2px;
                transition: 0.5s;
                text-align: center;
                cursor: pointer;
            }

            .submit-butt:hover {
                background-color: #f9cc62;
            }

            .register-butt {
                background-color: #153d4b;
                color: #fcfcfc;
                padding: 14px;
                border: none;
                border-radius: 8px;
                cursor: pointer;
                font-family: prefson-1;
                font-size: 20px;
                font-weight: bold;
                box-shadow: rgb(0 0 0 / 10%) 0px 4px 8px 0px;
                text-shadow: rgb(0 0 0 / 10%) 2px 2px;
                margin-top: 12px;
                transition: 0.5s;
                text-align: center;
                cursor: pointer;
            }

            .register-butt:hover {
                background-color: #1b4858;
            }

            .login-error {
                display: flex;
                background: #fdeeee;
                padding: 12px 16px;
                font-family: prefson-1;
                font-size: 12px;
                line-height: 17px;
                border-radius: 4px;
                border: 1px solid #e22820;
                color: #141111;
            }

            .login-error-icon {
                min-width: 20px;
                min-height: 20px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/error.png);
                margin-right: 12px;
            }

            @media (max-width: __BREAKPOINT-M-SMALL__) {
                .wrapper {
                    width: 100%;
                    border-radius: 0;
                }
            }

            @media (max-width: __BREAKPOINT-X-SMALL__) {
                .wrapper {
                    margin: 0px auto;
                    height: calc(100vh - 80px);
                }

                .header {
                    font-size: 16px;
                    padding: 24px 48px;
                }

                .container {
                    padding: 36px 48px 0;
                }

                .container-logo {
                    display: none;
                }

                .container-login {
                    width: 100%;
                }

                .login-buttons {
                    padding: 0 48px 48px;
                }

                .login-field {
                    font-size: 13px;
                }

                .submit-butt {
                    font-size: 18px;
                }

                .register-butt {
                    font-size: 18px;
                }

                .login-error {
                    margin-bottom: 16px;
                }
            }
		`;
	}

	render() {
		return html`
            <div class="wrapper">
                <div class="header">Sign in to Prefson</div>
                <div class="container">
                    <div class="container-login">
                        <input type="text" placeholder="Username" class="login-field" id="sn-username" @keypress=${this._onLoginPress.bind(this)}>
                        <input type="password" placeholder="Password" class="login-field" id="sn-password" @keypress=${this._onLoginPress.bind(this)}>
                        ${
                            this._loginError && !this._validError ? html`
                                <div class="login-error">
                                    <div class="login-error-icon"></div>
                                    <span style="margin: auto 0;">${this._loginError}</span>
                                </div>   
                            ` : ``
                        }
                        ${
                            this._validError ? html`
                                <div class="login-error">
                                    <div class="login-error-icon"></div>
                                    <span style="margin: auto 0;">${this._validError}</span>
                                </div>   
                            ` : ``
                        }
                    </div>
                    <div class="container-logo"></div>
                </div>

                <div class="login-buttons">
                    <div class="submit-butt" @click=${this._onLoginClick.bind(this)}>Log In</div>
                    <div class="register-butt" @click=${this._onRegisterClick.bind(this)}>Create New Account</div>
                </div>
            </div>
		`;
	}

    static get properties() {
        return {
            _loginError: {
                type: String
            },
            _validError: {
                type: String
            }
        }
    }

    constructor() {
        super();
        this._loginError = undefined;
        this._validError = undefined;
    }

    firstUpdated(_changedProperties) {
        super.firstUpdated(_changedProperties);
    }

    _onLoginClick(e) {
        const username = this.shadowRoot.getElementById('sn-username').value;
        const pass = this.shadowRoot.getElementById('sn-password').value;
        if (!username || !pass) {
            this._validError = 'Please, fill out all required fields.';
            return;
        }
        fetch(Server.URI + Server.Login, {
            method: "POST",
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(
                {
                    username: username,
                    password: sha256(pass).toString()
                }
            )
        })
        .then((blob) => blob.json())
        .then((data) => {
            const res = JSON.parse(data);
            if (res.success) {
                this.dispatchEvent(new CustomEvent('execute-login', {
                    detail: {
                        cx: res.cx,
                        cxToken: res.cxToken
                    }
                }));
                this._onLoginSocket(username);                
            } else {
                this._loginError = res.message;
                this._validError = undefined;
            }
        });
    }

    _onLoginSocket(username) {
        const manager = new Manager(Server.SocketsURI);
        const socket = manager.socket("/");
        socket.auth = {
            username: username
        };
        socket.connect();
    }

    _onLoginPress(e) {
        if (e.key === 'Enter') {
            this._onLoginClick();
        }
    }

    _onRegisterClick(e) {
        this.dispatchEvent(new CustomEvent('register-click'));
    }
}

window.customElements.define(SnLogin.is, SnLogin);
