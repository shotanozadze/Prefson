import { LitElement, html, css } from 'lit';
import { styleMap } from 'lit/directives/style-map.js';
import { Server } from '../common/constants';

class SnSearchUser extends LitElement {
	static get is() {
		return 'sn-search-user';
	}

	static get styles() {
		return css`
            .wrapper {
                display: flex;
                height: 64px;
                transition: all 0.5s ease 0s;
                background: #01181b;
                margin: 0 16px;
                margin-top: 16px;
                border-radius: 12px;
                border: 1px solid #0f2d37;
                box-shadow: 0px 0px 3px black;
                position: relative;
            }

            .wrapper:hover {
                background-color: #1b1d21;
            }

            .user-avatar {
                position: relative;
                width: 40px;
                height: 40px;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                border-radius: 10px;
                margin-top: 12px;
                margin-left: 16px;
            }

            .user-name-wrapper {
                display: flex;
                flex-direction: column;
                margin: 12px 16px;
                user-select: none;
            }

            .user-name {
                font-family: pbold-1;
                color: #eaedde;
                font-size: 17px;
                line-height: 21px;
            }

            .user-username {
                font-family: prefson-1;
                color: #71767a;
                font-size: 16px;
                margin-top: 2px;
            }

            .user-add-friend {
                width: 24px;
                height: 24px;
                position: absolute;
                background-repeat: no-repeat;
                background-position: center center;
                background-size: contain;
                background-image: url(/assets/images/add.png);
                right: 16px;
                top: 20px;
                cursor: pointer;
            }
		`;
	}

	render() {
		return html`
            <div class="wrapper">
                <div
                    class="user-avatar"
                    style="${styleMap({'background-image': 'url(' + (this.user.avatar || '/assets/images/user.png') + ')'})}">
                </div>
                <div class="user-name-wrapper">
                    <div class="user-name">${this.user.firstname + ' ' + this.user.lastname}</div>
                    <div class="user-username">${'@' + this.user.username}</div>
                </div>
                ${
                    !this.profile.connections.includes(this.user.username)
                        && this.user.username !== this.profile.username && !this._disableAdd ? html`
                        <div class="user-add-friend" @click=${() => this._connect()}></div>
                    ` : ``
                }
            </div>
		`;
	}

    static get properties() {
        return {
            user: {
                type: Object
            },
            profile: {
                type: Object
            },
            _disableAdd: {
                type: Boolean
            }
        };
    }

    constructor() {
        super();
        this.user = undefined;
        this._disableAdd = false;
    }

    _connect() {
        fetch(Server.URI + Server.ConnectUser, {
            method: "POST",
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(
                {
                    user1: this.profile.username,
                    user2: this.user.username
                }
            )
        })
        .then((blob) => blob.json())
        .then((data) => {
            const res = JSON.parse(data);
            if (res.success) {
                this._disableAdd = true;
            }
        });
    }
}

window.customElements.define(SnSearchUser.is, SnSearchUser);
