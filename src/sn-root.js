import { LitElement, html, css } from 'lit';
import { Router } from '@vaadin/router';
import routes from './routes';

class SnRoot extends LitElement {
    static get is() {
        return 'sn-root';
    }

    static get styles() {
		return css`
			#outlet {
				background-color: #011f26;
                min-height: 100vh;
			}
		`;
	}

    render() {
        return html`
            <div id="outlet"></div>
        `;
    }

    firstUpdated() {
        const router = new Router(this.shadowRoot.getElementById('outlet'), {
            baseUrl: '/'
        });
        router.setRoutes(routes);
    }
}

customElements.define(SnRoot.is, SnRoot);
